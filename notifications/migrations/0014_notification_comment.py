# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-10-27 15:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0027_auto_20161011_1356'),
        ('notifications', '0013_notification_is_seen'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='comment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='posts.PostComment'),
        ),
    ]
