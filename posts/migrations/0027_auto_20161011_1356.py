# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-10-11 13:56
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0026_remove_post_is_anonymous'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='postreport',
            name='post',
        ),
        migrations.RemoveField(
            model_name='postreport',
            name='user',
        ),
        migrations.DeleteModel(
            name='PostReport',
        ),
    ]
