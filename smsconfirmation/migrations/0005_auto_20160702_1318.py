# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-07-02 13:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('smsconfirmation', '0004_phoneconfirmation_request_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='phoneconfirmation',
            name='user',
        ),
        migrations.AddField(
            model_name='phoneconfirmation',
            name='phone',
            field=models.CharField(default='', max_length=20),
            preserve_default=False,
        ),
    ]
