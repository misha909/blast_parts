# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-10-29 14:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0039_auto_20161011_1356'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usersettings',
            name='notify_comments',
            field=models.IntegerField(choices=[(0, 'Off'), (1, 'People I follow'), (2, 'Everyone')], default=1),
        ),
        migrations.AlterField(
            model_name='usersettings',
            name='notify_new_followers',
            field=models.IntegerField(choices=[(0, 'Off'), (1, 'People I follow'), (2, 'Everyone')], default=1),
        ),
        migrations.AlterField(
            model_name='usersettings',
            name='notify_reblasts',
            field=models.IntegerField(choices=[(0, 'Off'), (1, 'People I follow'), (2, 'Everyone')], default=1),
        ),
    ]
