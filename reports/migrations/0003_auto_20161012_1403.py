# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-10-12 14:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0002_auto_20161011_1418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='reason',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Other'), (1, 'Sensitive content'), (2, 'Spam'), (3, 'Duplicated content'), (4, 'Bulling'), (5, 'Intel violation')], help_text='Report reason'),
        ),
    ]
